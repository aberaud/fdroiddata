Categories:Security
License:GPLv3
Web Site:
Source Code:https://github.com/aaronjwood/PortAuthority
Issue Tracker:https://github.com/aaronjwood/PortAuthority/issues
Donate:https://github.com/aaronjwood/PortAuthority/blob/HEAD/README

Auto Name:Port Authority
Summary:Port scanner
Description:
A handy systems and security-focused tool, Port Authority is a very fast port
scanner. It also allows you to quickly discover hosts on your network and will
display useful network information about your device and other hosts.
.

Repo Type:git
Repo:https://github.com/aaronjwood/PortAuthority

Build:1.2.2,5
    commit=v1.2.2
    subdir=app
    gradle=free
    prebuild=sed -i -e '/com.aaronjwood.portauthority.free/d' build.gradle

Build:1.2.5,8
    commit=v1.2.5
    subdir=app
    gradle=free
    prebuild=sed -i -e '/com.aaronjwood.portauthority.free/d' build.gradle

Build:1.3.0,9
    commit=v1.3.0
    subdir=app
    gradle=free
    prebuild=sed -i -e '/com.aaronjwood.portauthority.free/d' build.gradle

Build:1.3.1,10
    commit=v1.3.1
    subdir=app
    gradle=free
    prebuild=sed -i -e '/com.aaronjwood.portauthority.free/d' build.gradle

Build:1.3.2,11
    commit=v1.3.2
    subdir=app
    gradle=free
    prebuild=sed -i -e '/com.aaronjwood.portauthority.free/d' build.gradle

Build:1.4.0,12
    commit=v1.4.0
    subdir=app
    gradle=free
    prebuild=sed -i -e '/com.aaronjwood.portauthority.free/d' build.gradle

Build:1.5.0,13
    commit=v1.5.0
    subdir=app
    gradle=free
    prebuild=sed -i -e '/com.aaronjwood.portauthority.free/d' build.gradle

Build:1.5.2,14
    commit=v1.5.2
    subdir=app
    gradle=free
    prebuild=sed -i -e '/com.aaronjwood.portauthority.free/d' build.gradle

Build:1.5.5,16
    commit=v1.5.5
    subdir=app
    gradle=free
    prebuild=sed -i -e '/com.aaronjwood.portauthority.free/d' build.gradle

Build:1.5.7,18
    commit=v1.5.7
    subdir=app
    gradle=free
    prebuild=sed -i -e '/com.aaronjwood.portauthority.free/d' build.gradle

Build:1.6.0,19
    commit=v1.6.0
    subdir=app
    gradle=free
    prebuild=sed -i -e '/com.aaronjwood.portauthority.free/d' build.gradle

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.6.0
Current Version Code:19
