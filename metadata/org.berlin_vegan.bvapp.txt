Categories:Navigation
License:GPLv2
Web Site:http://www.berlin-vegan.org/app/index_en.html
Source Code:https://github.com/Berlin-Vegan/berlin-vegan-guide
Issue Tracker:https://github.com/Berlin-Vegan/berlin-vegan-guide/issues

Auto Name:Berlin-Vegan
Summary:Guide for vegan dining and shopping in Berlin
Description:
With the "Berlin Vegan Guide", you've got quick access to an overview of the
vegan food and shopping possibilities in Berlin, Germany --- even while you're
out and about.

The app contains the following information:

* over 300 restaurants, snack bars and takeaways with vegan declared offers
* a further 200 shopping possibilities in the area of cosmetics, groceries and organic produce

The guide is not just suitable for vegetarians and vegans but also for all
people who are trying to live their life in a greener and more animal friendly
way.
.

Repo Type:git
Repo:https://github.com/Berlin-Vegan/berlin-vegan-guide

Build:1.6,18
    commit=116c661d029f7f23cb2be4ad168dc8554dd16794
    subdir=platforms/android
    init=echo "# Just create the file" > project.properties
    srclibs=1:Cordova@3.2.0,YUICompressor@v2.4.8
    rm=lib/tools/ycompressor.jar,platforms/android/libs/cordova*jar,platforms/android/libs/phonegap*jar,platforms/android/local.properties,platforms/android/build.properties,platforms/android/default.properties,platforms/android/build.xml
    target=android-19
    build=pushd $$YUICompressor$$ && \
        ant && \
        popd && \
        cp $$YUICompressor$$/build/yuicompressor-2.4.8.jar ../../lib/tools/ycompressor.jar && \
        cd ../../ && \
        ant prod.android

Build:1.9,21
    commit=47f0850acf58e80e7f0d8ec75251408d6bcefddb
    subdir=platforms/android
    init=echo "# Just create the file" > project.properties
    srclibs=1:Cordova@3.2.0,YUICompressor@v2.4.8
    rm=lib/tools/ycompressor.jar,platforms/android/libs/cordova*jar,platforms/android/libs/phonegap*jar,platforms/android/local.properties,platforms/android/build.properties,platforms/android/default.properties,platforms/android/build.xml
    target=android-19
    build=pushd $$YUICompressor$$ && \
        ant && \
        popd && \
        cp $$YUICompressor$$/build/yuicompressor-2.4.8.jar ../../lib/tools/ycompressor.jar && \
        cd ../../ && \
        ant prod.android

Build:2.0.3-fdroid,24
    disable=remove apk to get in sync with upstream VC
    commit=2.0.3
    subdir=app
    gradle=foss
    prebuild=sed -i -e 's/versionName gitTag ?: gitBranch/versionName \"$$VERSION$$\"/g' -e 's/def gitDescription = dirtyWorkingCopy .*/def gitDescription = gitHash/g' -e '/playstoreCompile/d' build.gradle

Maintainer Notes:
* Versions <= 2.0 do not build from the current repo, but one needs to use the old repo <https://github.com/smeir/Berlin-Vegan-Guide>
* AUM wont work, since we rely on the versionname, which yet cannot be parsed from gradle: 
     Auto Update Mode:Version +-fdroid %v
.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:Unknown
Current Version Code:24
