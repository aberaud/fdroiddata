Categories:System
License:GPLv3
Web Site:http://termux.com
Source Code:https://github.com/termux/termux-widget
Issue Tracker:https://github.com/termux/termux-widget/issues

Auto Name:Termux:Widget
Summary:Launch Termux commands from the homescreen
Description:
Add-on app which adds shortcuts to [[com.termux]] scripts and commands on the
home screen. Scripts should be placed in the $HOME/.shortcuts/ folder to allow
quick access to frequently used commands without typing.
.

Repo Type:git
Repo:https://github.com/termux/termux-widget

Build:0.3,3
    commit=v0.3
    subdir=app
    gradle=yes

Build:0.4,4
    disable=pre-release

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.3
Current Version Code:3
